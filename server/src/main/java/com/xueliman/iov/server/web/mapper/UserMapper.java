package com.xueliman.iov.server.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xueliman.iov.server.web.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author zxg
 */
@Repository
public interface UserMapper extends BaseMapper<User> {


}
